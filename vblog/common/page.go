package common

func NewPageRequest() *PageRequest {
	return &PageRequest{
		PageSize:   10,
		PageNumber: 1,
	}
}

type PageRequest struct {
	// 分页的大小
	PageSize int
	// 当前页码
	PageNumber int
}

// 1 0
// 2 skip(1 * page_size)
// 3. skip(2 * page_size)
func (req *PageRequest) Offset() int {
	return (req.PageNumber - 1) * req.PageSize
}
