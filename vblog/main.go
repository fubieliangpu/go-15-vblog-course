package main

import (
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/go-course-project/go15/vblog/apps/token/api"
	"gitlab.com/go-course-project/go15/vblog/ioc"

	//.....
	"gitlab.com/go-course-project/go15/vblog/conf"

	// 业务对象注册
	_ "gitlab.com/go-course-project/go15/vblog/apps/token/impl"
	_ "gitlab.com/go-course-project/go15/vblog/apps/user/impl"
)

func main() {
	// 1. 加载配置
	configPath := os.Getenv("CONFIG_PATH")
	if configPath == "" {
		configPath = "etc/application.yaml"
	}
	if err := conf.LoadConfigFromYaml(configPath); err != nil {
		panic(err)
	}

	// 2. 初始化Ioc
	if err := ioc.Controller.Init(); err != nil {
		panic(err)
	}

	// // 把程序需要对象组织起来，组装成业务程序 来处理业务逻辑
	// // User BO
	// userServiceImpl := user.NewUserServiceImpl()
	// // Token BO
	// tokenServiceImpl := token.NewTokenServiceImpl(userServiceImpl)

	// token 模块子路有
	tokenApiHandler := api.NewTokenApiHandler()

	// 对外开放API
	// Engine 对 HTTP Server的一个封装 Run, 也就是整个架构里面协议服务器的简化版本
	r := gin.Default()
	// /api/v1 ---> root group
	// RouterGroup ---> gin.IRouter
	root := r.Group("/vblog/api/v1")
	// 凡事路有前缀是  /vblog/api/v1/tokens 到交给tokenApiHandler处理
	tokenApiHandler.Registry(root.Group("tokens"))
	if err := r.Run(":8080"); err != nil {
		panic(err)
	}
}
